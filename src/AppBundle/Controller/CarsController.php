<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Cars;
use AppBundle\Form\CarsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;


class CarsController extends Controller
{
    /**
     * @Route("/car", name="cars")
     */
    public function indexAction(){
        return $this->render('@App/cars/index.html.twig',[
            'cars' => $this->getDoctrine()->getRepository("AppBundle:Cars")->findAll()
        ]);
    }

    /**
     * @Route("/car/{id}/show", name="car_view", requirements={"id" : "[0-9]+"})
     */
    public function showAction($id){
        return $this->render('@App/cars/show.html.twig', [
            'cars' => $this->getDoctrine()->getRepository("AppBundle:Cars")->find($id)
        ]);
    }
    /**
     * @Route("/car/{id}/edit", name="car_edit", requirements={"id" : "[0-9]+"})
     */
    public function editAction(Request $request, Cars $cars){
        $form = $this->createForm(CarsType::class, $cars);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();
            $this->addFlash('success', 'Изменено!');
            return $this->redirectToRoute('car_edit', ['id' => $cars->getId()]);
        }

        return $this->render('@App/cars/edit.index.twig', [
            'cars_form' => $form->createView(),
            'car' => $cars
        ]);
    }

    /**
     * @Route("/car/{id}/delete", name="car_delete", requirements={"id" : "[0-9]+"})
     */
    public function deleteAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $elem = $em->getRepository('AppBundle:Cars')->find($id);
        $em->remove($elem);
        $em->flush();
        $this->addFlash('success', 'Удалено!');
        return $this->redirectToRoute('cars');
    }


    /**
     * @Route("/car/create", name="car_create")
     */
    public function createAction(Request $request){
        $form = $this->createForm(CarsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $feedback = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($feedback);
            $em->flush();
            $this->addFlash('success', 'Сохранено!');
            return $this->redirectToRoute('car_create');
        }

        return $this->render('@App/cars/create.index.twig', [
            'cars_form' => $form->createView()
        ]);
    }

}